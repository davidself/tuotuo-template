(defproject tuotuo-template/lein-template "0.1.0"
  :description "a template for creating applications"
  :url "https://github.com/davidself/tuotuo-template"
  :license {:name "MIT License"
            :url "http://opensource.org/licenses/MIT"}
  :eval-in-leiningen true)
